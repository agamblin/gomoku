cmake_minimum_required(VERSION 3.12)
project(pbrain-PARIS-Breui.Amaury)

set(CMAKE_CXX_STANDARD 14)

add_executable(pbrain-PARIS-Breui.Amaury sources/main.cpp sources/Game.cpp headers/Game.hpp sources/Brain.cpp headers/Brain.hpp sources/Board.cpp headers/Board.hpp sources/Cell.cpp headers/Cell.hpp sources/Position.cpp headers/Position.hpp)