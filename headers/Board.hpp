//
// Created by Arthur Gamblin on 09/11/2018.
//

#ifndef GOMOKU_BOARD_HPP
#define GOMOKU_BOARD_HPP

#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include "Cell.hpp"

class Board {
public:
    Board();
    virtual ~Board();

    void    setBoard(int size);
    int     size() const;
    void    addMove(Position pos, int player = SELF);
    void    addMove(int x, int y, int player = SELF);
    void    addMoveBoard(int x, int y, int player);
    void    reset();
    std::vector<std::vector<Cell> >     chest();

private:
    int                                                 _size;
    std::vector<std::vector<Cell> >                     _chest;

    void                    fillChest();
};


#endif //GOMOKU_BOARD_HPP
