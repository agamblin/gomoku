//
// Created by Arthur Gamblin on 09/11/2018.
//

#ifndef GOMOKU_CELL_HPP
#define GOMOKU_CELL_HPP

#include "Position.hpp"

#define NOONE       0
#define SELF        1
#define OPPONENT    2

class Cell {
public:
    Cell();
    Cell(Position   pos);
    Cell(int x, int y);
    virtual         ~Cell();

    bool operator==(const Cell &rhs) const;
    bool operator!=(const Cell &rhs) const;

    Position        pos()   const;
    bool            empty() const;
    int             player() const;
    void            setEmpty(bool empty);
    void            setPosition(Position pos);
    void            setPosition(int x, int y);
    void            setPlayer(int player);

private:
    Position                    _pos;
    bool                        _empty;
    int                         _player;
};


#endif //GOMOKU_CELL_HPP
