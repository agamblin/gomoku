//
// Created by Arthur Gamblin on 09/11/2018.
//

#ifndef GOMOKU_GAME_HPP
#define GOMOKU_GAME_HPP

#include "Brain.hpp"

#define start   1
#define begin   2
#define turn    3
#define board   4
#define about   5
#define end     6

class Game {
public:
    Game();
    virtual ~Game();
    void     launchGame();

private:
    std::string                 _buffer;
    std::unique_ptr<Board>      _board;
    std::unique_ptr<Brain>      _brain;

    int                         getCmd();
    void                        dispatcher();
    Position                    getPos();
    std::vector<std::string>    split(const std::string &delim, const std::string& str);
};


#endif //GOMOKU_GAME_HPP
