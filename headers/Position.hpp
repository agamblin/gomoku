//
// Created by Arthur Gamblin on 09/11/2018.
//

#ifndef GOMOKU_POSITION_HPP
#define GOMOKU_POSITION_HPP


class Position {
public:
    Position();
    Position(int x, int y);
    virtual ~Position();

    bool operator==(const Position &rhs) const;
    bool operator!=(const Position &rhs) const;

    int     x() const;
    int     y() const;
    void    setX(int x);
    void    setY(int y);

private:
    int             _x;
    int             _y;
};


#endif //GOMOKU_POSITION_HPP
