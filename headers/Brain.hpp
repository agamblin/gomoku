//
// Created by Arthur Gamblin on 09/11/2018.
//

#ifndef GOMOKU_BRAIN_HPP
#define GOMOKU_BRAIN_HPP

#include "Board.hpp"

#define                     BLOCK_ONE       0
#define                     BLOCK_TWO       50
#define                     BLOCK_THREE     250
#define                     BLOCK_FOUR      500
#define                     BLOCK_FIVE      9000

#define                     ONE_IN_A_ROW    0
#define                     TWO_IN_A_ROW    100
#define                     THREE_IN_A_ROW  300
#define                     FOUR_IN_A_ROW   400
#define                     FIVE_IN_A_ROW   10000

class Brain {
public:
    explicit                Brain(std::unique_ptr<Board> &board);
    virtual                 ~Brain();
    void                    setBoardSize(int size);
    std::vector<Position>   potentialMoves();
    void                    playIt();
    void                    reset();
private:
    std::unique_ptr<Board> &_board;
    std::vector<Position>   _potentialMoves;
    int                     _attack[5];
    int                     _defense[5];

    void                    addToPotentialMoves(int x, int y);
    void                    calculateBestMove();
    void                    removeFromVector(Position pos);
    bool                    checkIfPresentInVector(Position pos);

    int                     upperRow(Position, int idx = 0, int player = 0);
    int                     bottomRow(Position, int idx = 0, int player = 0);
    int                     rightRow(Position, int idx = 0, int player = 0);
    int                     leftRow(Position, int idx = 0, int player = 0);
    int                     upperRightRow(Position, int idx = 0, int player = 0);
    int                     upperLeftRow(Position, int idx = 0, int player = 0);
    int                     bottomRightRow(Position, int idx = 0, int player = 0);
    int                     bottomLeftRow(Position, int idx = 0, int player = 0);
};


#endif //GOMOKU_BRAIN_HPP
