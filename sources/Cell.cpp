//
// Created by Arthur Gamblin on 09/11/2018.
//

#include <iostream>
#include "../headers/Cell.hpp"

Cell::Cell() = default;

Cell::Cell(Position pos) : _pos(Position(pos.x(), pos.y())) {
}

Cell::Cell(int x, int y) : _pos(Position(x, y)) {
    _empty = true;
    this->_player = NOONE;
}

Cell::~Cell() = default;

Position    Cell::pos() const {
    return (_pos);
}

bool        Cell::empty() const{
    return (_empty);
}

int         Cell::player() const {
    return (_player);
}

void        Cell::setEmpty(bool empty) {
    this->_empty = empty;
}

void        Cell::setPosition(Position pos) {
    this->_pos = pos;
}

void        Cell::setPosition(int x, int y) {
    this->_pos.setX(x);
    this->_pos.setY(y);
}

void        Cell::setPlayer(int player) {
    this->_player = player;
    if (player != NOONE)
        this->setEmpty(false);
}

bool Cell::operator==(const Cell &rhs) const {
    return _pos == rhs._pos &&
           _empty == rhs._empty &&
           _player == rhs._player;
}

bool Cell::operator!=(const Cell &rhs) const {
    return !(rhs == *this);
}
