//
// Created by Arthur Gamblin on 09/11/2018.
//

#include "../headers/Game.hpp"

Game::Game() : _board(new Board), _brain(new Brain(_board)) {
}

Game::~Game() = default;


void                    Game::launchGame() {
    while (std::getline(std::cin, this->_buffer)) {
        dispatcher();
    }
}

Position                Game::getPos() {
    Position            pos;
    std::string         posInStr;

    posInStr = this->split(" ", this->_buffer)[1];
    pos.setX(std::stoi(this->split(",", posInStr)[0]) - 1);
    pos.setY(std::stoi(this->split(",", posInStr)[1]) - 1);
    return (pos);
}

int                         Game::getCmd() {
    if (this->_buffer.find("START ", 0) != std::string::npos) {
        return start;
    } else if (this->_buffer.find("BEGIN", 0) != std::string::npos) {
        return begin;
    } else if (this->_buffer.find("TURN", 0) != std::string::npos) {
        return turn;
    } else if (this->_buffer.find("BOARD", 0) != std::string::npos) {
        return board;
    } else if (this->_buffer.find("ABOUT", 0) != std::string::npos) {
        return about;
    } else if (this->_buffer.find("END", 0) != std::string::npos) {
        return end;
    }
    else
        return 0;
}

std::vector<std::string>    Game::split(const std::string &delim, const std::string& str) {
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;

    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());
    return tokens;
}

void                        Game::dispatcher() {
    switch (this->getCmd()) {
        case start: {
            int size = std::stoi(this->split(" ", this->_buffer)[1]);
            this->_board->setBoard(size);
            std::cout << "OK\n\r";
        } break;
        case begin: {
            this->_board->addMove(this->_board->size() / 2, this->_board->size() / 2);
        } break;
        case turn: {
            this->_board->addMove(this->getPos(), OPPONENT);
            this->_brain->playIt();
        } break;
        case board: {
            std::string line;
            int x, y, player;

            while (std::getline(std::cin, line)) {
                if (line == "END")
                    break;
                x = std::stoi(this->split(",", line)[0]);
                y = std::stoi(this->split(",", line)[1]);
                player = std::stoi(this->split(",", line)[2]);
                this->_board->addMoveBoard(x, y, player);
                line.clear();
            }
            this->_brain->playIt();
        } break;
        case about: {
            std::cout << "name=\"pbrain-PARIS-Breuil.Amaury\", version=\"1.0\", author=\"amaury.breuil@epitech.eu\", country=\"FR\"\r\n";
        } break;
        case end: {
            this->_board->reset();
            this->_brain->reset();
            return ;
        }
        default:
            break;

    }
    this->_buffer.clear();
}
