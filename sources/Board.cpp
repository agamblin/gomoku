//
// Created by Arthur Gamblin on 09/11/2018.
//

#include "../headers/Board.hpp"

                                Board::Board() = default;

                                Board::~Board() = default;

void                            Board::setBoard(int size) {
    this->_size = size;
    this->fillChest();
}

int                             Board::size() const {
    return (this->_size);
}

void                            Board::fillChest() {
    this->_chest.resize((unsigned long)this->size(), std::vector<Cell>((unsigned long)this->size()));
    for (int x = 0; x < this->size(); x++) {
        for (int y = 0; y < this->size(); y++) {
            this->_chest[x][y] = Cell(x, y);
        }
    }
}

void                            Board::addMove(Position pos, int player) {
    this->_chest[pos.x()][pos.y()].setPlayer(player);
    if (player == SELF) {
        std::cout << pos.x() << "," << pos.y() << "\r\n";
    }
}

void                            Board::addMove(int x, int y, int player) {
    this->_chest[x][y].setPlayer(player);
    if (player == SELF) {
        std::cout << x + 1 << "," << y + 1 << "\r\n";
    }
}

void                            Board::addMoveBoard(int x, int y, int player) {
    this->_chest[x][y].setPlayer(player);
}

std::vector<std::vector<Cell> > Board::chest() {
    return (_chest);
}

void                            Board::reset() {
    this->_chest.clear();
}
