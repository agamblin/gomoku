//
// Created by Arthur Gamblin on 09/11/2018.
//

#include "../headers/Position.hpp"

Position::Position() = default;

Position::Position(int x, int y) : _x(x), _y(y) {

}

Position::~Position() = default;

int Position::x() const {
    return (_x);
}

int Position::y() const {
    return (_y);
}

void Position::setX(int x) {
    this->_x = x;
}
void Position::setY(int y) {
    this->_y = y;
}

bool Position::operator==(const Position &rhs) const {
    return _x == rhs._x &&
           _y == rhs._y;
}

bool Position::operator!=(const Position &rhs) const {
    return !(rhs == *this);
}

