//
// Created by Arthur Gamblin on 09/11/2018.
//

#include "../headers/Brain.hpp"

Brain::Brain(std::unique_ptr<Board> &board) : _board(board){
    _attack[0] = ONE_IN_A_ROW;
    _attack[1] = TWO_IN_A_ROW;
    _attack[2] = THREE_IN_A_ROW;
    _attack[3] = FOUR_IN_A_ROW;
    _attack[4] = FIVE_IN_A_ROW;

    _defense[0] = BLOCK_ONE;
    _defense[1] = BLOCK_TWO;
    _defense[2] = BLOCK_THREE;
    _defense[3] = BLOCK_FOUR;
    _defense[4] = BLOCK_FIVE;
}


Brain::~Brain() = default;

void                Brain::setBoardSize(int size) {
    this->_board->setBoard(size);
}

void                Brain::addToPotentialMoves(int x, int y) {
    if (y+1 < this->_board->size() && this->_board->chest()[x][y+1].empty()) {
        //if (!this->checkIfPresentInVector(Position(x, y+1))) {
            this->_potentialMoves.emplace_back(Position(x, y + 1));
        //}
    } if (y-1 >= 0 && this->_board->chest()[x][y-1].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x, y-1))) {
            this->_potentialMoves.emplace_back(Position(x, y - 1));
      //  }
    } if (x+1 < this->_board->size() && this->_board->chest()[x+1][y].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x+1, y))) {
            this->_potentialMoves.emplace_back(Position(x + 1, y));
      //  }
    } if (x-1 >= 0 && this->_board->chest()[x-1][y].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x-1, y))) {
            this->_potentialMoves.emplace_back(Position(x - 1, y));
      //  }
    } if (x-1 >= 0 && y-1 >= 0 && this->_board->chest()[x-1][y-1].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x-1, y-1))) {
            this->_potentialMoves.emplace_back(Position(x - 1, y - 1));
      //  }
    } if (x-1 >= 0 && y+1 < this->_board->size() && this->_board->chest()[x-1][y+1].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x-1, y+1))) {
            this->_potentialMoves.emplace_back(Position(x - 1, y + 1));
     //   }
    } if (x+1 < this->_board->size() && y+1 < this->_board->size() && this->_board->chest()[x+1][y+1].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x+1, y+1))) {
            this->_potentialMoves.emplace_back(Position(x + 1, y + 1));
      //  }
    } if (x+1 < this->_board->size() && y-1 >= 0 && this->_board->chest()[x+1][y-1].empty()) {
      //  if (!this->checkIfPresentInVector(Position(x+1, y-1))) {
            this->_potentialMoves.emplace_back(Position(x + 1, y - 1));
     //   }
    }
}

void                    Brain::removeFromVector(Position pos) {
    for (std::vector<Position>::iterator it = this->_potentialMoves.begin(); it != _potentialMoves.end(); it++) {
        if (it->x() == pos.x() && it->y() == pos.y()) {
            this->_potentialMoves.erase(it);
            break;
        }
    }
}

bool                    Brain::checkIfPresentInVector(Position pos) {
    for (auto &_potentialMove : _potentialMoves) {
      if (_potentialMove == pos)
          return (true);
    }
    return (false);
}

void                    Brain::calculateBestMove() {
    int         bestWeight = 0;
    int         tmp = 0;
    Position    bestMove(0, 0);

    for (auto &_potentialMove : _potentialMoves) {
        tmp = upperRow(_potentialMove, 0) + bottomRow(_potentialMove, 0)
                + leftRow(_potentialMove, 0) + rightRow(_potentialMove, 0) +
                upperRightRow(_potentialMove, 0) + upperLeftRow(_potentialMove, 0) +
                bottomLeftRow(_potentialMove, 0) + bottomRightRow(_potentialMove, 0);
        if (tmp > bestWeight) {
            bestWeight = tmp;
            bestMove.setX(_potentialMove.x());
            bestMove.setY(_potentialMove.y());
        }
    }
    this->_board->addMove(bestMove);
}

void                    Brain::playIt()  {
    for (int x = 0; x < this->_board->size(); x++) {
        for (int y = 0; y < this->_board->size(); y++) {
            if (!this->_board->chest()[x][y].empty())
                this->addToPotentialMoves(x, y);
        }
    }
    this->calculateBestMove();
    this->_potentialMoves.clear();
}

int                     Brain::upperRow(Position pos, int idx, int player) {
    if (pos.y() + 1 < this->_board->size() && !(this->_board->chest()[pos.x()][pos.y()+1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x()][pos.y() + 1].player();
        }
        if (pos.y() + 1 < this->_board->size() && this->_board->chest()[pos.x()][pos.y() + 1].player() == player) {
            pos.setY(pos.y() + 1);
            return (upperRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::bottomRow(Position pos, int idx, int player) {

    if (pos.y() - 1 >= 0 && !(this->_board->chest()[pos.x()][pos.y()-1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x()][pos.y() - 1].player();
        }
        if (pos.y() - 1 >= 0 && this->_board->chest()[pos.x()][pos.y() - 1].player() == player) {
            pos.setY(pos.y() - 1);
            return (bottomRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::rightRow(Position pos, int idx, int player) {
    if (pos.y() + 1 < this->_board->size() && !(this->_board->chest()[pos.x() + 1][pos.y()].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() + 1][pos.y()].player();
        }
        if (pos.y() + 1 < this->_board->size() && this->_board->chest()[pos.x() + 1][pos.y()].player() == player) {
            pos.setX(pos.x() + 1);
            return (rightRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::leftRow(Position pos, int idx, int player) {
    if (pos.x() - 1 >= 0 && !(this->_board->chest()[pos.x() - 1][pos.y()].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() - 1][pos.y()].player();
        }
        if (pos.x() - 1 >= 0 && this->_board->chest()[pos.x() - 1][pos.y()].player() == player) {
            pos.setX(pos.x() - 1);
            return (leftRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::upperRightRow(Position pos, int idx, int player) {
    if (pos.x() + 1 < this->_board->size() && pos.y() + 1 < this->_board->size()
    && !(this->_board->chest()[pos.x() + 1][pos.y() + 1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() + 1][pos.y() + 1].player();
        }
        if (pos.x() + 1 < this->_board->size() && pos.y() + 1 < this->_board->size() &&
            this->_board->chest()[pos.x() + 1][pos.y() + 1].player() == player) {
            pos.setY(pos.y() + 1);
            pos.setX(pos.x() + 1);
            return (upperRightRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::upperLeftRow(Position pos, int idx, int player) {
    if (pos.x() - 1 >= 0 && pos.y() + 1 < this->_board->size()
    && !(this->_board->chest()[pos.x() - 1][pos.y() + 1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() - 1][pos.y() + 1].player();
        }
        if (pos.x() - 1 >= 0 && pos.y() + 1 < this->_board->size() &&
            this->_board->chest()[pos.x() - 1][pos.y() + 1].player() == player) {
            pos.setY(pos.y() + 1);
            pos.setX(pos.x() - 1);
            return (upperLeftRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::bottomRightRow(Position pos, int idx, int player) {
    if (pos.x() + 1 < this->_board->size() && pos.y() - 1 >= 0
        && !(this->_board->chest()[pos.x() + 1][pos.y() - 1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() + 1][pos.y() - 1].player();
        }
        if (pos.x() + 1 < this->_board->size() && pos.y() - 1 >= 0 &&
            this->_board->chest()[pos.x() + 1][pos.y() - 1].player() == player) {
            pos.setY(pos.y() - 1);
            pos.setX(pos.x() + 1);
            return (bottomRightRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

int                     Brain::bottomLeftRow(Position pos, int idx, int player) {
    if (pos.x() - 1 >= 0 && pos.y() - 1 >= 0
        && !(this->_board->chest()[pos.x() - 1][pos.y() - 1].empty())) {
        if (idx == 0) {
            player = this->_board->chest()[pos.x() - 1][pos.y() - 1].player();
        }
        if (pos.x() - 1 >= 0 && pos.y() - 1 >= 0 &&
            this->_board->chest()[pos.x() - 1][pos.y() - 1].player() == player) {
            pos.setY(pos.y() - 1);
            pos.setX(pos.x() - 1);
            return (bottomLeftRow(pos, idx + 1, player));
        }
    }
    if (player == SELF)
        return (this->_attack[idx]);
    return (this->_defense[idx]);
}

std::vector<Position>   Brain::potentialMoves() {
    return (_potentialMoves);
}

void                    Brain::reset() {
    this->_board->chest().clear();
    this->_potentialMoves.clear();
}
